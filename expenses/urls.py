"""expenses URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from receipts.views import (
    receipt_list,
    create_receipt,
    expensecategory_list,
    account_list,
    create_category,
    create_account,
)
from django.shortcuts import redirect
from accounts.views import user_login, user_logout, signup


def redirect_to_home(request):
    return redirect("home")


urlpatterns = [
    path("admin/", admin.site.urls),
    path("receipts/" "", receipt_list, name="home"),
    path("", redirect_to_home),
    path("accounts/login/", user_login, name="login"),
    path("accounts/logout/", user_logout, name="logout"),
    path("accounts/signup/", signup, name="signup"),
    path("receipts/create/", create_receipt, name="create_receipt"),
    path("receipts/categories/", expensecategory_list, name="category_list"),
    path("receipts/accounts/", account_list, name="account_list"),
    path(
        "receipts/categories/create/", create_category, name="create_category"
    ),
    path("receipts/accounts/create/", create_account, name="create_account"),
]
